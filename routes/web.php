<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('currencies', ['as' => 'currencies', 'uses' => 'Currency\CurrencyController@currencies']);
Route::get('currencies/get', ['as' => 'currencies.get', 'uses' => 'Currency\CurrencyController@get']);

Route::get('twitter', ['as' => 'twitter', 'uses' => 'Twitter\TwitterController@twitter']);
Route::get('twitter/get', ['as' => 'twitter.get', 'uses' => 'Twitter\TwitterController@get']);
// Route::get('twitter/token', ['as' => 'twitter.token', 'uses' => 'Twitter\TwitterController@token']);