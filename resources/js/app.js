
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import {ClientTable} from 'vue-tables-2';

Vue.use(ClientTable, {}, false, 'bootstrap4');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('currencies-table-component', require('./components/CurrenciesTableComponent.vue'));
Vue.component('twitter-map-component', require('./components/TwitterMapComponent.vue'));

const app = new Vue({
    el: '#app'
});

centrifuge.connect();