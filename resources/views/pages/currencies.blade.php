<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Currencies Page</title>
        <link href="/css/slim.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="slim-mainpanel" id="app">
	      <div class="container">
	        <div class="slim-pageheader">
	          <ol class="breadcrumb slim-breadcrumb">
	            <li class="breadcrumb-item"><a href="#">Главная</a></li>
	            <li class="breadcrumb-item active">Таблица валют</li>
	          </ol>
	          <h6 class="slim-pagetitle">Таблица валют</h6>
	        </div><!-- slim-pageheader -->

	        <div class="section-wrapper">
	          <label class="section-title">Данные таблицы обновляются каждые 15 секунд</label>
	          <currencies-table-component></currencies-table-component>
	        </div><!-- section-wrapper -->

	      </div><!-- container -->
	    </div><!-- slim-mainpanel -->
    </body>
    <script src="/js/app.js" type="text/javascript"></script>
</html>
