<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Twitter Page</title>
        <link href="/css/slim.min.css" rel="stylesheet">
    </script>
    </head>
    <body>
        <div class="slim-mainpanel" id="app">
	      <div class="container">
	        <div class="slim-pageheader">
	          <ol class="breadcrumb slim-breadcrumb">
	            <li class="breadcrumb-item"><a href="#">Главная</a></li>
	            <li class="breadcrumb-item active">Страница твиттов</li>
	          </ol>
	          <h6 class="slim-pagetitle">Страница твиттов</h6>
	        </div><!-- slim-pageheader -->

	        <div class="section-wrapper">
	          <label class="section-title">Твитты людей c локацией в Нью-Йорке</label>
	          <twitter-map-component></twitter-map-component>
	        </div><!-- section-wrapper -->

	      </div><!-- container -->
	    </div><!-- slim-mainpanel -->
    </body>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="/js/app.js" type="text/javascript"></script>
</html>
