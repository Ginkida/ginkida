<?php

namespace App\Http\Controllers\Currency;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    public function __construct()
    {

    }

    public function currencies()
    {
        return view('pages.currencies');
    }

    public function get()
    {
        $currencies = json_decode(file_get_contents('http://phisix-api3.appspot.com/stocks.json'), true);

        $array = [];

        foreach ($currencies['stock'] as $cur) {
            $array[] = [
                'name' => $cur['name'],
                'amount' => round($cur['price']['amount'], 2),
                'volume' => $cur['volume']
            ];
        }

        return response()->json($array);
    }

     
}
