<?php

namespace App\Http\Controllers\Twitter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\TwitterService;
use LaraComponents\Centrifuge\Centrifuge;

class TwitterController extends Controller
{
    protected $tweetService;
    protected $centrifuge;

    public function __construct(TwitterService $tweetService, Centrifuge $centrifuge)
    {
        $this->tweetService = $tweetService;
        $this->centrifuge = $centrifuge;
    }

    public function twitter()
    {
        return view('pages.twitter');
    }

    public function get()
    {
        $tweets = $this->tweetService->get_array();

        return response()->json($tweets);
    }

    public function token()
    {
        $time = time();
        $token = $this->centrifuge->generateToken(1, $time);

        return ['user' => 1, 'time' => $time, 'token' => $token];
    }
}
