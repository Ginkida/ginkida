<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use TwitterStreamingApi;
use App\Services\TwitterService;

class ListenForHashTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:listen-for-hash-tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen for New York tweets';

    protected $twitterService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TwitterService $twitterService)
    {
        parent::__construct();

        $this->twitterService = $twitterService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TwitterStreamingApi::publicStream()
            ->whenFrom([[-74, 40, -73, 41]], function (array $tweet) {
                if (isset($tweet['geo']) && isset($tweet['user']['profile_background_image_url_https'])) {
                    $this->twitterService->create($tweet);
                }
            })
            ->startListening();
    }
}
