<?php

namespace App\Services;

use App\Model\Tweet;
use LaraComponents\Centrifuge\Centrifuge;
use DB;

class TwitterService
{
	public function __construct(Centrifuge $centrifuge)
    {
        $this->centrifuge = $centrifuge;
    }

    public function create(array $array)
    {
    	$tweet = new Tweet;
    	$tweet->tweet_id = $array['id'];
    	$tweet->name = $array['user']['name'];
        $tweet->text = $array['text'];
    	$tweet->image = $array['user']['profile_image_url_https'];
    	$tweet->coordinates = serialize([$array['geo']['coordinates'][0], $array['geo']['coordinates'][1]]);
    	$tweet->save();

        $this->centrifuge($tweet);
    }

    public function get_array()
    {
        $tweets = Tweet::get();

        $tweets = $this->decorate($tweets);

        return $tweets;
    }

    private function centrifuge(Tweet $tweet)
    {
        $this->centrifuge->publish('twitter', [
            'tweet' => [
                'id' => $tweet->id,
                'tweet_id' => $tweet->tweet_id,
                'text' => $tweet->text,
                'name' => $tweet->name,
                'image' => $tweet->image,
                'coordinates' => unserialize($tweet->coordinates)
            ]
        ]);
    }

    private function decorate($collection)
    {
        $array = [];

        foreach ($collection as $tweet) {
            $array[] = [
                'id' => $tweet->id,
                'tweet_id' => $tweet->tweet_id,
                'text' => $tweet->text,
                'name' => $tweet->name,
                'image' => $tweet->image,
                'coordinates' => unserialize($tweet->coordinates)
            ];
        }

        return $array;
    }


}